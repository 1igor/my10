//
//  NewsCard.swift
//  my10
//
//  Created by hex dyachuk on 22.06.2020.
//  Copyright © 2020 Monochrome Coop. All rights reserved.
//

import SwiftUI

struct NewsCard: View {
    var backgroundImage = Image("PlaceholderImage")
    var thumbnailImage = Image("PlaceholderImage")
    var body: some View {
        ZStack {
            backgroundImage.resizable().scaledToFill().mask(RoundedRectangle(cornerRadius: 10)).frame(height: 80)
//            RoundedRectangle(cornerRadius: 10).background(backgroundImage.resizable().scaledToFill())
            RoundedRectangle(cornerRadius: 10).foregroundColor(Color("NewsCardColor"))
            
        }.frame(height: 80)
    }
}

struct NewsCard_Previews: PreviewProvider {
    static var previews: some View {
        NewsCard()
    }
}
